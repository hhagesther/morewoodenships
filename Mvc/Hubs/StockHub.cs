﻿using System;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Repo;

namespace Mvc.Hubs
{
    public class StockHub : Hub
    {
        
        public void GetStock()
        {
            var repo = new StockRepo();
            var stocks = repo.GetAllStocks();
            Clients.All.setStocks(JsonConvert.SerializeObject(stocks));
        }

        
        public void SetStock(string ticker, string newPrice)
        {
            var repo = new StockRepo();
            var stock = repo.GetStock(ticker);
            stock.PreviousPrice = stock.Price;
            stock.Price = Convert.ToDecimal(newPrice);
            stock.LastUpdated = DateTime.Now;
            repo.SaveStock(stock);
            
            // Send stock to all clients
            Clients.All.updateStock(JsonConvert.SerializeObject(stock));
        }
    }
}
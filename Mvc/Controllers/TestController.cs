﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;

namespace Mvc.Controllers
{
    public class TestController : ApiController
    {
        [GET("test")]
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("Du kom igjennom! Hurra for deg! :)") };
        }
    }
}

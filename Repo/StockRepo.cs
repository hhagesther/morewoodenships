﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Raven.Client.Document;

namespace Repo
{
    public class StockRepo
    {
        private readonly DocumentStore _documentStore;

        public StockRepo()
        {
            _documentStore = new DocumentStore
                    {
                        Url = "http://halhag-dpc:8081/",
                        DefaultDatabase = "Stocks"
                    };
            _documentStore.Initialize();
        }

        public Stock GetStock(string ticker)
        {
            using (var session = _documentStore.OpenSession())
            {
                var results = from stock in session.Query<Stock>()
                              where stock.Ticker == ticker
                              select stock;
                var stocks = results.ToList();
                return stocks.Count == 0 ? null : stocks[0];
            }
        }

        public void SaveStock(Stock stock)
        {
            using (var session = _documentStore.OpenSession())
            {
                var results = from existingStock in session.Query<Stock>()
                              where existingStock.Ticker == stock.Ticker
                              select existingStock;
                var existingStocks = results.ToList();
                if (existingStocks.Count > 0)
                {
                    session.Delete(existingStocks[0]);
                
                }
                session.Store(stock);
                session.SaveChanges();
            }
        }

        public List<Stock> GetAllStocks()
        {
            using (var session = _documentStore.OpenSession())
            {
                var results = from stock in session.Query<Stock>()
                              orderby stock.Ticker descending 
                              select stock;
                var stocks = results.ToList();
                return stocks;
            }
        }
    }
}

﻿using System;

namespace Domain
{
    public class Stock
    {
        public string Name { get; set; }
        public string Ticker { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdated { get; set; }
        public decimal PreviousPrice { get; set; }
    }
}